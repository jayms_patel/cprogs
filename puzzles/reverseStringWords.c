/*
 *  How to reverse a sentence with a program?
 *  Program crashes if we use char *str, Why?
 *  check: http://c-faq.com/decl/strlitinit.html
 *
 */

#include <stdio.h>
#include <string.h>

static void
reverseWord(char *str, int start, int end)
{
    int i, j;
    char ch;

    for (i = start, j = end; i <= j; i++, j--) {
        ch = str[j];
        str[j] = str[i];
        str[i] = ch;
    }
}

int main(int argc, char **argv)
{
    int i = 0;
    unsigned int start = 0, end = 0;
    char *pStart, *pEnd;
    char ch, str[] = "Pls reverse me!";

    printf("After reverse: %s\n", str);
    pStart = &str[0];
    for (i = 0; i <= strlen(str); i++) {
        if (str[i] == ' ' || str[i] == '\0') {
            pEnd = &str[i];
            end = start + ((int ) (pEnd - pStart) - 1);
            reverseWord(str, start, end);
            start = i + 1;
            pStart = &str[start];
        }
    }
    printf("After reverse: %s\n", str);

    return 0;
}
