#include <stdio.h>

int main(int argc, char **argv)
{
    int i, j, k;

    for (i=71; i>=65; i--) {
        for (j=65; j<=i; j++) {
            printf("%c ", j);
        }
        for (k=i-1; k>=65; k--) {
            printf("%c ", k);
        }
        printf("\n");
    }
}
