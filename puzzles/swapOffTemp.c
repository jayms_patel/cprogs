/*
 *  Swap two varaibale without using third temp variable
 *  We can accomplish it with arithmentic operators combinations (+-), (/*) and XOR ^ operator
 *
 */
#include <stdio.h>

static void
swapUsingXor(int *x, int *y)
{
    int a = *x, b = *y;

    printf("Before a = %d, b = %d\n", a, b);
    a = a ^ b;
    b = a ^ b;
    a = a ^ b;

    *x = a;
    *y = b;
}

static void
swapUsingMultipication(int *x, int *y)
{
    int a = *x, b = *y;

    printf("Before a = %d, b = %d\n", a, b);
    a = a * b;
    b = a / b;
    a = a / b;

    *x = a;
    *y = b;
}

static void
swapUsingAddition(int *x, int *y)
{
    int a = *x, b = *y;

    printf("Before a = %d, b = %d\n", a, b);
    a = a + b;
    b = a - b;
    a = a - b;

    *x = a;
    *y = b;
}

int main()
{
    int a, b;

    printf("Enter Value of a: ");
    scanf("%d", &a);
    printf("Enter Value of b: ");
    scanf("%d", &b);

    printf("Use Addition\n");
    swapUsingAddition(&a, &b);
    printf("After a = %d, b = %d\n", a, b);

    printf("Use Multipication\n");
    swapUsingMultipication(&a, &b);
    printf("After a = %d, b = %d\n", a, b);

    printf("Use XOR\n");
    swapUsingXor(&a, &b);
    printf("After a = %d, b = %d\n", a, b);

    return 0;
}
