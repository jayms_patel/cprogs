/*
 *  How to reverse a sentence with a program?
 *  Program crashes if we use char *str, Why?
 *  check: http://c-faq.com/decl/strlitinit.html
 *
 */

#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
    char ch, str[] = "Pls reverse me!";
    int i, j, strLen;

    printf("After reverse: %s\n", str);

    strLen = strlen(str) - 1;
    for (i = 0, j = strLen; i <= j; i++, j--) {
        ch = str[j];
        str[j] = str[i];
        str[i] = ch;
    }

    printf("After reverse: %s\n", str);

    return 0;
}
