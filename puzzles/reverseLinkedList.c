#include <stdio.h>
#include <stdlib.h>

struct linkedList {
    int data;
    struct linkedList *link;
};

struct linkedList *
addNode()
{
    struct linkedList *tmp = (struct linkedList *) malloc(sizeof(struct linkedList));
    return ((tmp) ? tmp : NULL);
}

static int
insertNode(struct linkedList **list, int data)
{
    struct linkedList *tmp = addNode();

    if (tmp) {
        tmp->data = data;
        tmp->link = NULL;
    } else {
        return 0;
    }

    if (*list) {
        tmp->link = *list;
    }
    *list = tmp;

    return 1;
}

static void
reverseList(struct linkedList **list)
{
    struct linkedList *current = *list, *next = NULL, *last = NULL;

    while(current) {
        next = current->link;
        current->link = last;
        last = current;
        current = next;
    }
    *list = last;
}

static void
displayList(struct linkedList *list)
{
    struct linkedList *tmp = list;
    while (tmp) {
        printf("%d ", tmp->data);
        tmp = tmp->link;
    }
    printf("\n");
}

static void
distroyList(struct linkedList *list)
{
    struct linkedList *tmp;

    while (list) {
        tmp = list;
        list = list->link;
        free(tmp);
    }
}

int main()
{
    int i, data;
    struct linkedList *list = NULL;

    for(i = 0; i < 5; i++) { 
        printf("Insert node (%2d): ", i);
        scanf("%d", &data);
        insertNode(&list, data);
    }
    displayList(list);
    reverseList(&list);
    displayList(list);
    distroyList(list);
}
