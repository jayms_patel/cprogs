#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>

int param_test;
module_param(param_test, int, S_IRUSR | S_IWUSR);

static int param_init(void)
{
	printk(KERN_ALERT "Showing the parameter demo\n");
	printk(KERN_ALERT "Value of parameter is: %d\n", param_test);

	return 0;
}

static void param_exit(void)
{
	printk(KERN_ALERT "Exiting the parameter demo\n");
}

module_init(param_init);
module_exit(param_exit);
