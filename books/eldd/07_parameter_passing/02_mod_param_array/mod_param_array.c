#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>

int param_array[3];
module_param_array(param_array, int, NULL, S_IWUSR | S_IRUSR);

static int array_init(void)
{
	printk(KERN_ALERT "Into the parameter array\n");
	printk(KERN_ALERT "Array elements are: %d\t%d\t%d\n", param_array[0], param_array[1], param_array[2]);

	return 0;
}

static void array_exit(void)
{
	printk(KERN_ALERT "Exiting the array parameter demo\n");
}

module_init(array_init);
module_exit(array_exit);

MODULE_LICENSE("GPL");
