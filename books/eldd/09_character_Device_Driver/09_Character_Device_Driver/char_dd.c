#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

#define DEVICE_NAME	"char_dd"
#define DEVICE_COUNT 3

static dev_t first;

static int char_dd_init(void)
{
	printk(KERN_INFO "NAMASKAR: %s\n", DEVICE_NAME);

	if ((alloc_chrdev_region(&first, 0, DEVICE_COUNT, DEVICE_NAME)) < 0) {
		goto err;
	}

	printk("<Major,Minor>:<%d,%d>\n", MAJOR(first), MINOR(first));

	return 0;

err:
	return -1;
}

static void char_dd_exit(void)
{
	unregister_chrdev_region(first, DEVICE_COUNT);
	printk("Device unregistered\n");
}

module_init(char_dd_init);
module_exit(char_dd_exit);

MODULE_LICENSE("GPL");
