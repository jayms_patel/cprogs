#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>

static int process_init(void)
{
	struct task_struct *task;

	for_each_process(task) {
		printk("Process Name: %s\tPID: %d\tProcess State:%ld\n", task->comm, task->pid, task->state);
	}

	printk("Current: Process Name: %s\tPID: %d\tProcess State:%ld\n", current->comm, current->pid, current->state);

	return 0;
}

static void process_exit(void)
{
	printk(KERN_INFO "Clearing Up\n");
}

module_init(process_init);
module_exit(process_exit);

MODULE_LICENSE("GPL");

