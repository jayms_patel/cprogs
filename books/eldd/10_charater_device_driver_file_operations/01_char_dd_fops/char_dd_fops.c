#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>

#define DEVICE_NAME	"char_dd_fops"
#define DEVICE_CLASS_NAME	"char_dd_fops_class"
#define DEVICE_FILE_NAME "my_char_dev"
#define DEVICE_COUNT	3

static dev_t first;
static struct class *cl;
static struct cdev c_dev;

static int my_open(struct inode *i, struct file *f)
{
	printk(KERN_INFO "Driver:open()\n");
	return 0;
}

static int my_close(struct inode *i, struct file *f)
{
	printk(KERN_INFO "Driver:close()\n");
	return 0;
}

static ssize_t my_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
	printk(KERN_INFO "Driver:read()\n");
	return 0;
}

static ssize_t my_write(struct file *f, const char __user *buf, size_t len, loff_t *off)
{
	printk(KERN_INFO "Driver:read()\n");
	return 0;
}

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_close,
	.read = my_read,
	.write = my_write
};

static int __init char_dd_fops_init(void)
{
	printk(KERN_INFO "Namaskar: %s\n", DEVICE_NAME);

	if (alloc_chrdev_region(&first, 0, DEVICE_COUNT, DEVICE_NAME) < 0) {
		goto err;
	}

	if ((cl = class_create(THIS_MODULE, DEVICE_CLASS_NAME)) == NULL) {
		goto err_1;
	}

	if (device_create(cl, NULL, first, NULL, "%s", DEVICE_FILE_NAME) == NULL) {
		goto err_2;
	}

	cdev_init(&c_dev, &fops);
	if (cdev_add(&c_dev, first, DEVICE_COUNT) < 0) {
		goto err_3;
	}

	return 0;

err_3:
	device_destroy(cl, first);
err_2:
	class_destroy(cl);
err_1:
	unregister_chrdev_region(first, DEVICE_COUNT);
err:
	return -1;
}

static void __exit char_dd_fops_exit(void)
{
	device_destroy(cl, first);
	class_destroy(cl);
	unregister_chrdev_region(first, DEVICE_COUNT);
	printk("Unregistered %s\n", DEVICE_NAME);
}

module_init(char_dd_fops_init);
module_exit(char_dd_fops_exit);

MODULE_LICENSE("GPL");
