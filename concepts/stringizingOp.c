/*
 *  This program explains Stringizing Operator (#).
 *  that replaces contents as it is passed
 */

#include <stdio.h>

#define fun(str)    printf(#str"\n");
#define nofun(str)  printf("%s\n", str);

int main()
{
    printf("TEST1: ");
    fun(int);
    printf("TEST2: ");
    fun("This is new line");
    printf("TEST3: ");
    nofun("This is new line");
    return 0;
}
