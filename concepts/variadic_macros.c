/*
 *  This program explains usage of Variadic Macros.
 *  https://gcc.gnu.org/onlinedocs/cpp/Variadic-Macros.html
 *
 *  1.  fun1()  '__VA_ARGS__' is macro that replaces all the arguments in its place
 *  2.  fun2()  To use more descriptive name instead of '__VA_ARGS__', we can write argument name
 *              immediate before '...', that name would be used as variable argument
 *  3.  fun3()  We can have named argument, but in that we need to pass atleast one argument
 *              Than can be a format and other are varialbles
 *  4.  fun4()  '##' token paste operator has a special meaning when placed between a comma and a
 *              variable argument. If the variable argument is left out when the macro is used, 
 *              then the comma before the '##' will be deleted. This does not happen if its an empty
 *              argument, nor does it happen if the token preceding '##' is anything other than a comma. 
 */

#include <stdio.h>

#define fun1(...)            printf("JAYMIN " __VA_ARGS__)
#define fun2(args...)       printf(args)
#define fun3(format, ...)   printf(format, __VA_ARGS__);
#define fun4(format, ...)   printf(format, ## __VA_ARGS__);

int main()
{
    fun1("%d:%s\n", 10, "Jaymin");
    fun2("%d:%s\n", 10, "Jaymin");
    fun3("%s:%d:%s\n", "Success", 10, "Jaymin");
    fun4("%s:%d:%s\n", "Success", 10, "Jaymin");
    fun4("Success\n");
    return 0;
}
