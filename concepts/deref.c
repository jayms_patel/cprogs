/*
 *
 * Use of deference operator and address of operator with precedence
 * example
 *
 */

#include <stdio.h>

int main(int argc, char **argv)
{
	int x = 7;
	int *y = &x;

	printf("*y = %d\n", *y);
	printf("x = %d\n", x);
	printf("(*y)++ = %d\n", (*y)++);
	printf("x = %d\n", x);
	printf("*y++ = %d\n", *y++);
	printf("x = %d\n", x);
	printf("*(y++) = %d\n", *(y++));
	printf("x = %d\n", x);
}
