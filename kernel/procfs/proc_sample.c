#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

unsigned int count = 0;

static int proc_sample_show(struct seq_file *m, void *v)
{
	seq_printf(m, "%u", count);
	count++;
	return 0;
}

static int proc_sample_open(struct inode *inode, struct file *file)
{
	return single_open(file, proc_sample_show, NULL);
}

static const struct file_operations proc_sample_fops = {
	.owner = THIS_MODULE,
	.open = proc_sample_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

struct proc_dir_entry *test;
static int __init init_proc_sample(void)
{
	test = proc_mkdir("test", NULL);
	proc_create("proc_sample", 0666, test, &proc_sample_fops);
	return 0;
}

static void __exit exit_proc_sample(void)
{
	remove_proc_entry("proc_sample", test);
	remove_proc_entry("test", NULL);
	return;
}

module_init(init_proc_sample);
module_exit(exit_proc_sample);

MODULE_AUTHOR("Jaymin Patel");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Procfs sample module");
