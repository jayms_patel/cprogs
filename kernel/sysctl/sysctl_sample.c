#include <linux/module.h>
#include <linux/sysctl.h>

int busy_ontime = 0;
int busy_offtime = HZ;
char busy_string[128] = {0};

int min = 0;
int max = 100;

static struct ctl_table busy_table[4] = {
	{
		.procname = "timestring",
		.data = &busy_string,
		.maxlen = sizeof(busy_string),
		.mode = 0644,
		.proc_handler = proc_dostring,
	},

	{
		.procname = "ontime",
		.data = &busy_ontime,
		.maxlen = sizeof(busy_ontime),
		.mode = 0644,
		.proc_handler = proc_dointvec_minmax,
		.extra1 = &min,
		.extra2 = &max,
	},
	{
		.procname = "offtime",
		.data = &busy_offtime,
		.maxlen = sizeof(busy_offtime),
		.mode = 0644,
		.proc_handler = proc_dointvec,
	},
	{}
};

static struct ctl_table busy_kern_table[2] = {
	{
		.procname = "busy",
		.mode = 0555,
		.child = busy_table,
	},
	{}
};

static struct ctl_table busy_root_table[2] = {
	{
		.procname = "sysctl_sample",
		.mode = 0555,
		.child = busy_kern_table,
	},
	{}
};

static struct ctl_table_header *busy_table_header;  

static int __init init_sysctl_sample(void)
{
	printk(KERN_INFO "Starting sysctl sample\n");
	busy_table_header = register_sysctl_table(busy_root_table);
	if (!busy_table_header) {
		return -ENOMEM;
	}
	return 0;
}

static void __exit exit_sysctl_sample(void)
{
	printk(KERN_INFO "Exiting sysctl sample\n");
	unregister_sysctl_table(busy_table_header);
}

module_init(init_sysctl_sample);
module_exit(exit_sysctl_sample);

MODULE_AUTHOR("Jaymin Patel");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Sysctl Interface Example");
