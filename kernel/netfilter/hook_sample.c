#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>

int hook_sample(unsigned int hooknum, struct sk_buff **skb, 
		const struct net_device *in, const struct net_device *out,
		int (*okfn) (struct sk_buff *))
{
	printk(KERN_INFO "Received packet\n");
	return NF_ACCEPT;
}

static struct nf_hook_ops nf_hook_sample = {
	.hook = hook_sample,
	.pf = NFPROTO_IPV4,
	.hooknum = 	NF_INET_PRE_ROUTING,
	.priority = NF_IP_PRI_FIRST,
};

static int __init init_hook_sample(void)
{
	printk("Starting hook_sample\n");
	nf_register_hook(&nf_hook_sample);
	return 0;
}

static void __exit exit_hook_sample(void)
{
	printk("Exiting hook_sample\n");
	nf_unregister_hook(&nf_hook_sample);
}

module_init(init_hook_sample);
module_exit(exit_hook_sample);

MODULE_AUTHOR("Jaymin Patel");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Exmaple netfilter hook");
