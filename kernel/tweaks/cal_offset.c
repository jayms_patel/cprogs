#include <stdio.h>

struct foo_bar {
	unsigned int foo;
	char bar;
	char boo;
};

int main(int argc, char **argv)
{
	struct foo_bar tmp;

	printf("Address of &tmp = %p\n", &tmp);
	printf("Address and offset of tmp.foo, address = %p, offset = %lu\n", &tmp.foo, (unsigned long) (&(((struct foo_bar *)0)->foo)));
	printf("Address and offset of tmp.bar, address = %p, offset = %lu\n", &tmp.bar, (unsigned long) (&(((struct foo_bar *)0)->bar)));
	printf("Address and offset of tmp.boo, address = %p, offset = %lu\n", &tmp.boo, (unsigned long) (&(((struct foo_bar *)0)->bar)));

	printf("Base Address of structure from tmp->foo = %p\n", (&tmp.foo - (unsigned long) (&(((struct foo_bar *)0)->foo))));
	printf("Base Address of structure from tmp->bar = %p\n", (&tmp.bar - (unsigned long) (&(((struct foo_bar *)0)->bar))));
	printf("Base Address of structure from tmp->boo = %p\n", (&tmp.boo - (unsigned long) (&(((struct foo_bar *)0)->boo))));
}
