#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netdevice.h>

static int notifier_sample_notify(struct notifier_block *nb, unsigned long event, void *data)
{
	struct net_device *dev = netdev_notifier_info_to_dev(data);
	if (!dev) {
		printk(KERN_ERR "no device found\n");
		return NOTIFY_DONE;
	}

	switch (event) {
		case NETDEV_UP:
			printk(KERN_INFO "Device: %s, event: NETDEV_UP\n", dev->name);
			break;
		case NETDEV_DOWN:
			printk(KERN_INFO "Device: %s, event: NETDEV_DOWN\n", dev->name);
			break;
		default:
			printk(KERN_INFO "Device: %s, event: %lu\n", dev->name, event);
	}

	return NOTIFY_OK;
}

static struct notifier_block notifier_sample_nb = {
	.notifier_call = notifier_sample_notify,
};

static int __init init_notifier_sample(void)
{
	printk(KERN_INFO "Start testing notifier\n");
	register_netdevice_notifier(&notifier_sample_nb);
	return 0;
}

static void __exit exit_notifier_sample(void)
{
	unregister_netdevice_notifier(&notifier_sample_nb);
	printk(KERN_INFO "Stop testing notifier\n");
}

module_init(init_notifier_sample);
module_exit(exit_notifier_sample);

MODULE_AUTHOR("Jaymin Patel");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Notifier Example");
