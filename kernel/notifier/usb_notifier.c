#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>

static int notifier_sample_notify(struct notifier_block *nb, unsigned long event, void *data)
{
	struct usb_device *dev = (struct usb_device *) (data);
	if (!dev) {
		printk(KERN_ERR "no device found\n");
		return NOTIFY_DONE;
	}

	printk(KERN_INFO "Device: %s %s, event: %lu\n", dev->product, dev->manufacturer, event);

	return NOTIFY_OK;
}

static struct notifier_block notifier_sample_nb = {
	.notifier_call = notifier_sample_notify,
};

static int __init init_notifier_sample(void)
{
	printk(KERN_INFO "Start testing notifier\n");
	usb_register_notify(&notifier_sample_nb);
	return 0;
}

static void __exit exit_notifier_sample(void)
{
	usb_unregister_notify(&notifier_sample_nb);
	printk(KERN_INFO "Stop testing notifier\n");
}

module_init(init_notifier_sample);
module_exit(exit_notifier_sample);

MODULE_AUTHOR("Jaymin Patel");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("USB Notifier Example");
