#include <stdio.h>
#include <stdlib.h>
#include "list.h"

struct kool_list {
	struct list_head list;
	int to;
	int from;
};

int main(int argc, char **argv)
{
	struct kool_list *tmp;
	struct list_head *pos, *q;
	unsigned int i;
	struct kool_list mylist;

	/* Initialize list */
	INIT_LIST_HEAD(&(mylist.list));

	/* Insert data in list */
	for (i = 5; i != 0; --i) {
		tmp = malloc(sizeof(struct kool_list));
		printf("Enter to and from: ");
		scanf("%d %d", &tmp->to, &tmp->from);
		list_add(&(tmp->list), &(mylist.list));
	}

	printf("Linked list traversal:\n");
	/* Travers list */
	list_for_each(pos, &(mylist.list)) {
		tmp = list_entry(pos, struct kool_list, list);
		printf("to = %d, from: %d\n", tmp->to, tmp->from);
	}

	printf("Linked list reverse traversal:\n");
	/* Travers list */
	list_for_each_prev(pos, &(mylist.list)) {
		tmp = list_entry(pos, struct kool_list, list);
		printf("to = %d, from: %d\n", tmp->to, tmp->from);
	}

	printf("Traversal using list_for_each_entry\n");
	list_for_each_entry(tmp, &(mylist.list), list) {
		printf("to = %d, from: %d\n", tmp->to, tmp->from);
	}

	printf("Delete linked list\n");
	list_for_each_safe(pos, q, &(mylist.list)) {
		tmp = list_entry(pos, struct kool_list, list);
		printf("Freeing Item: to = %d, from = %d\n", tmp->to, tmp->from);
		list_del(pos);
		free(tmp);
	}
}
