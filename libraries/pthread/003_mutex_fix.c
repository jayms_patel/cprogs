#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

pthread_t tid[2];
int counter;
pthread_mutex_t lock;

void * do_something(void *arg)
{
	pthread_mutex_lock(&lock);
	unsigned long i = 0;

	counter += 1;

	printf("Job %d started\n", counter);

	for (i = 0; i < 0xFFFFFFFF; i++);

	printf("Job %d finished\n", counter);

	pthread_mutex_unlock(&lock);

	return NULL;
}

int main(void)
{
	int i = 0;
	int err;
	int *ptr[2];

	if (pthread_mutex_init(&lock, NULL) != 0) {
		printf("mutex init failed\n");
		return 1;
	}

	while (i < 2) {
		err = pthread_create(&tid[i], NULL, &do_something, NULL);
		if (err) {
			printf("Can't create thread %d(%m)\n", i);
		} else {
			printf("Thread created successfully\n");
		}
		i++;
	}

	pthread_join(tid[0], (void **) &ptr[0]);
	pthread_join(tid[1], (void **) &ptr[1]);

	pthread_mutex_destroy(&lock);
	
	return 0;
}
