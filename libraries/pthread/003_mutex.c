#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

pthread_t tid[2];
int counter;

void * do_something(void *arg)
{
	unsigned long i = 0;

	counter += 1;

	printf("Job %d started\n", counter);

	for (i = 0; i < 0xFFFFFFFF; i++);

	printf("Job %d finished\n", counter);

	return NULL;
}

int main(void)
{
	int i = 0;
	int err;
	int *ptr[2];

	while (i < 2) {
		err = pthread_create(&tid[i], NULL, &do_something, NULL);
		if (err) {
			printf("Can't create thread %d(%m)\n", i);
		} else {
			printf("Thread created successfully\n");
		}
		i++;
	}

	pthread_join(tid[0], (void **) &ptr[0]);
	pthread_join(tid[1], (void **) &ptr[1]);
	
	return 0;
}
