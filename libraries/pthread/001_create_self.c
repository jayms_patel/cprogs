#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

pthread_t tid[2];

void * do_something(void *arg)
{
	unsigned long i = 0;

	pthread_t id = pthread_self();

	if (pthread_equal(tid[0], id)) {
		printf("First thread processing\n");
	} else {
		printf("Second thread processing\n");
	}

	for (i = 0; i < 0xFFFFFFFF; i++);

	return NULL;
}

int main(void)
{
	int i = 0;
	int err;

	while (i < 2) {
		err = pthread_create(&tid[i], NULL, &do_something, NULL);
		if (err) {
			printf("Can't create thread %d(%m)\n", i);
		} else {
			printf("Thread created successfully\n");
		}
		i++;
	}
	
	sleep(5);

	return 0;
}
