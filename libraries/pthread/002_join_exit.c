#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

pthread_t tid[2];
int ret1, ret2;

void * do_something(void *arg)
{
	unsigned long i = 0;

	pthread_t id = pthread_self();

	if (pthread_equal(tid[0], id)) {
		printf("First thread processing\n");
		ret1 = 100;
		pthread_exit(&ret1);
	} else {
		printf("Second thread processing\n");
		ret2 = 200;
		pthread_exit(&ret2);
	}

	for (i = 0; i < 0xFFFFFFFF; i++);

	return NULL;
}

int main(void)
{
	int i = 0;
	int err;
	int *ptr[2];

	while (i < 2) {
		err = pthread_create(&tid[i], NULL, &do_something, NULL);
		if (err) {
			printf("Can't create thread %d(%m)\n", i);
		} else {
			printf("Thread created successfully\n");
		}
		i++;
	}

	pthread_join(tid[0], (void **) &ptr[0]);
	pthread_join(tid[1], (void **) &ptr[1]);
	
	printf("Return value from first thread is: %d\n", *ptr[0]);
	printf("Return value from second thread is: %d\n", *ptr[1]);

	return 0;
}
