#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void null_test_success(void **state)
{
}

int main(int argc, char **argv)
{
	const UnitTest tests[] = {
		unit_test(null_test_success),
	};

	return run_tests(tests);
}
