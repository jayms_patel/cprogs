#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

extern void leak_memory(void);
extern void buffer_overflow(void);
extern void buffer_underflow(void);

void leak_memory_test(void **state)
{
	leak_memory();
}

void buffer_overflow_test(void **state)
{
	buffer_overflow();
}

void buffer_underflow_test(void **state)
{
	buffer_underflow();
}

int main(int argc, char **argv)
{
	struct UnitTest tests[] = {
		unit_test(leak_memory_test),
		unit_test(buffer_overflow_test),
		unit_test(buffer_underflow_test),
	};

	return run_tests(tests);
}
