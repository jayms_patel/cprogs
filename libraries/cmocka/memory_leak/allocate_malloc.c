#include <malloc.h>

#ifdef UNIT_TESTING
extern void * _test_malloc(const size_t size, const char *file, const int line);
extern void * _test_calloc(const size_t number_of_element, const size_t size,
		const char *file, const int line);
extern void _test_free(void *ptr, const char *file, const int line);
#define malloc(size)	_test_malloc(size, __FILE__, __LINE__)
#define calloc(n, size)	_test_calloc(n, size, __FILE__, __LINE__)
#define free(ptr)		_test_free(ptr, __FILE__, __LINE__)
#endif

void leak_memory(void)
{
	int *const temporary = (int *) malloc(sizeof(int));
	*temporary = 0;
}

void buffer_overflow(void)
{
	char *const memory = (char *) malloc(sizeof(int));
	memory[sizeof(int)] = '!';
	free(memory);
}

void buffer_underflow(void)
{
	char *const memory = (char *) malloc(sizeof(int));
	memory[-1] = '!';
	free(memory);
}
