#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

extern int production_code(int money, const char *bank);

int __wrap_deposit(int money, const char *bank)
{
	check_expected(money);
	check_expected_ptr(bank);

	bool is_valid_account = mock_type(bool);
	if (!is_valid_account) { return EXIT_FAILURE; }

	int minimum_money = mock_type(int);
	if (minimum_money <= 100) { return EXIT_FAILURE; }

	char *bank_name = mock_type(char *);
	if (strcmp(bank_name, "Nordea") == 0) { return EXIT_FAILURE; }

	return EXIT_SUCCESS;
}

void test_successful_deposit(void **state)
{
	(void) state;

	int deposit_money = 200;
	const char *deposit_bank = "Citibank";

	expect_value(__wrap_deposit, money, deposit_money);
	expect_string(__wrap_deposit, bank, deposit_bank);

	will_return(__wrap_deposit, true);
	will_return(__wrap_deposit, deposit_money);
	will_return(__wrap_deposit, deposit_bank);
	
	int ret = production_code(deposit_money, deposit_bank);
	assert_int_equal(ret, EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
	struct UnitTest tests[] = {
		unit_test(test_successful_deposit),
	};

	return run_tests(tests);
}
