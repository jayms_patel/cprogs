#include <stdlib.h>
#include "deposit.h"

int production_code(int money, const char *bank)
{
	int ret = EXIT_FAILURE;

	if (bank) {
		ret = deposit(money, bank);
	}

	return ret;
}
