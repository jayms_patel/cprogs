typedef struct DatabaseConnection DatabaseConnection;

typedef unsigned int (*QueryDatabase) (
		DatabaseConnection * const connection, const char * const query_string,
		void *** const results);

struct DatabaseConnection {
	const char *url;
	unsigned int port;
	QueryDatabase query_database;
};

DatabaseConnection * connect_to_database(const char * const url,
		const unsigned int port);
