#include <stddef.h>
#include <stdio.h>
#include "database.h"

DatabaseConnection * connect_to_customer_database()
{
	return connect_to_database("customer.abcd.org", 321);
}

unsigned int get_customer_id_by_name(DatabaseConnection * const connection,
		const char * const customer_name)
{
	char query_string[256];
	int number_of_results;
	void **results;

	snprintf(query_string, sizeof(query_string),
			"SELECT ID FROM CUSTOMER WHERE NAME = %s", customer_name);
	number_of_results = connection->query_database(connection, query_string, &results);

	if (number_of_results != 1) {
		return -1;
	}

	return (unsigned int) results[0];
}
