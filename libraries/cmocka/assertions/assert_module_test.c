#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

extern void increment_value(int *const value);

void increment_value_fail(void **state)
{
	increment_value(NULL);
}

void increment_value_assert(void **state)
{
	expect_assert_failure(increment_value(NULL));
}

void decrement_value_fail(void **state)
{
	expect_assert_failure(decrement_value(NULL));
}

int main(int argc, char **argv)
{
	const UnitTest tests[] = {
		unit_test(increment_value_fail),
		unit_test(increment_value_assert),
		unit_test(decrement_value_fail),
	};

	return run_tests(tests);
}
