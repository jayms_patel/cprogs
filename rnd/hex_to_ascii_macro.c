#include <stdio.h>


#define STR "%x:%x:%x:%x"
#define HEX2STR(hex) hex[0], hex[1], hex[2], hex[3]

int main()
{
    unsigned char ascii[4];
    char temp[10];

    ascii[0] = 0x4a;
    ascii[1] = 0xe6;
    ascii[2] = 0xd7;
    ascii[3] = 0x4d;

    // sprintf(temp, STR, HEX2STR(ascii));
    printf(STR "\n", HEX2STR(ascii));

    return 0;
}
