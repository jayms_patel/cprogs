#include <stdio.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>

#define MAC             "%02x:%02x:%02x:%02x:%02x:%02x"
#define MAC2STR(mac)    mac[0], mac[1], mac[2], mac[3], mac[4], mac[5] 

int main(int argc, char **argv)
{
    struct ifreq ifr;
    char buf[1024];
    int success = 0;

    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sock == -1) {
        fprintf(stderr, "Couldn't open socket\n");
        return 1;
    }

    strcpy(ifr.ifr_name, argv[1]);

    if (ioctl(sock, SIOCGIFHWADDR, &ifr) == 0) {
        success = 1;
    } else {
        fprintf(stderr, "Couldn't run ioctl\n");
        return 1;
    }

    if (success) {
        unsigned char mac[6];
        int i;
        memcpy(mac, ifr.ifr_hwaddr.sa_data, 6);
        printf(MAC"\n", MAC2STR(mac));
    }

    return 0;
}
