#include <stdio.h>

int main()
{
    unsigned char ascii[4];
    char temp[10];

    ascii[0] = 0x4a;
    ascii[1] = 0xe6;
    ascii[2] = 0xd7;
    ascii[3] = 0x4d;

    sprintf(temp, "%x:%x:%x:%x", ascii[0], ascii[1],ascii[2], ascii[3]);
    printf("This is element %s\n", temp);

    return 0;
}
