#include <stdio.h>

int main(int argc, char **argv)
{
    int a[6];
    unsigned char m[6];

    sscanf(argv[1], "%x:%x:%x:%x:%x:%x", a, a+1, a+2, a+3, a+4, a+5);

    int i;

    for (i = 0; i<6; i++) {
        m[i] = a[i];
    }

    for (i = 0; i<6; i++) {
        printf("%02x", m[i]);
    }
}
