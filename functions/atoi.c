/*
 * Example explains detailed usage of atoi routine
 * It requires stdlib.h to be included in c file
 * usage: function converts the initial portion of the string (char *) to int.
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int n;
	char ch[1024];

	printf("Enter String: ");
	scanf("%s", ch);

	if ((n = atoi(ch)))
		printf("Converted String \"%s\" to integer is \"%d\"\n", ch, n);
	else
		printf("No integer string found in \"%s\"\n", ch);

	return 0;	
}
