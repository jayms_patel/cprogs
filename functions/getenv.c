/*
 * This program explains how one can use getenv() api to get value of shell environment variable
 * on success returns pointer of string
 * on failure returns null
 * getenv() needs stdlib.h
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    const char *home = getenv("HOME");
    const char *unknown = getenv("UNKNOWN");

    printf("Home variable: %s\n", home);
    printf("Unknown variable: %s\n", unknown);

    return 0;
}
