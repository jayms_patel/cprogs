#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define IPV4_ADDR "192.168.1.1"
#define IPV6_ADDR "fd06:333::307:5322:ef3a:b414"
#define IPV4	16885952
//#define IPV6(ip)	ip[0] = 0x330306fd; ip[1] = 0x0; ip[2] = 0x22530703; ip[3] = 0x14b43aef
#define IPV6(ip)	ip[0] = 0x02ff; ip[1] = 0x0; ip[2] = 0x0; ip[3] = 0x0; ip[4] = 0x0; ip[5] = 0x0; ip[6] = 0x0100, ip[7] = 0x0300

int main()
{
	char str[INET_ADDRSTRLEN];
	char str6[INET6_ADDRSTRLEN];
	struct in_addr ip;
//	int ip6[4];
	uint16_t ip6[8];
	int i;

	inet_pton(AF_INET, IPV4_ADDR, (void *) (&ip));
	printf("IP in network mode: %d\n", ip.s_addr);
	inet_ntop(AF_INET, (void *) (&ip), str, INET_ADDRSTRLEN);
	printf("IP Address: %s\n", str);

#if 0
	inet_pton(AF_INET6, IPV6_ADDR, (void *) (&ip6));
	for (i = 0; i < 4; i++) {
		printf("0x%x ", ip6.__in6_u.__u6_addr32[i]);
	}
#endif

	IPV6(ip6);
	inet_ntop(AF_INET6, (void *)(ip6), str6, INET6_ADDRSTRLEN);
	printf("IP6 Address: %s\n", str6);

	return 0;
}
