/*
 *  This program is example of signal() function which actions on signal received on process
 *  We can use SIG_IGN as a singla handler to ignore signal.
 */

#include <stdio.h>
#include <signal.h>
#include <unistd.h>

static void 
signalhandler(int signal)
{
    printf("Recevied signal: %d\n", signal);
    return;
}

int main(int argc, char **argv)
{
    while (1) {
        signal(SIGINT, SIG_IGN);
        // signal(SIGINT, signalhandler);
        sleep(1);
    }

    return 0;
}
