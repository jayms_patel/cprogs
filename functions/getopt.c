#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char **argv)
{
    int c;
    char A[1024]= {0};
    int B = 0;

    c = getopt(argc, argv, "A:B:CD");
    
    if (c < 0) {
        fprintf(stderr, "No options\n");
        exit(1);
    }

    switch(c) {
        case 'A':
            strcpy(A, optarg);
            break;
        case 'B':
            B = atoi(optarg);
            break;
        defalt:
            ;;       
    }

    printf("A: %s, B: %d\n", A, B);
}
