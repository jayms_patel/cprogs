#include <stdio.h>
#include <string.h>

#define BUFSIZE_1024    1024

enum parenthesis {
    ENUM_OPEN_PARENTHECE    = '(',
    ENUM_CLOSE_PARENTHECE   = ')',
    ENUM_OPEN_BRACE         = '{',
    ENUM_CLOSE_BRACE        = '}',
    ENUM_OPEN_BRACKET       = '[',
    ENUM_CLOSE_BRACKET      = ']'
};

enum operator {
    ENUM_OP_MODULO      = '%',
    ENUM_OP_SUBTRACT    = '-',
    ENUM_OP_SUM         = '+',
    ENUM_OP_DEVIDE      = '/',
    ENUM_OP_MULTIPLY    = '*'
};

enum operatorPrecedence {
    ENUM_SUBTRACT = 1,
    ENUM_SUM,
    ENUM_MULTIPLY,
    ENUM_DEVIDE,
    ENUM_MODULO
};

static int
isStackEmpty(int head)
{
    return ((head <= 0) ? 1 : 0);
}

static int
isStackFull(int head)
{
    return ((head >= BUFSIZE_1024) ? 1 : 0);
}

static int
topOfStack(char stack[], int head, char *data)
{
    if (isStackEmpty(head)) {
        printf("Stack Empty");
        return 0;
    }

    *data = stack[head - 1];
    return 1;
}

static int
push(char stack[], int *head, char data)
{
    if (isStackFull(*head)) {
        printf("Stack Full\n");
        return 0;
    }
    stack[(*head)++] = data;
    return 1;
}

static int
pop(char stack[], int *head)
{
    if (isStackEmpty(*head)) {
        printf("Stack Empty\n");
        return 0;
    }
    (*head)--;
    return 1;
}

static int
isParenthesis(char parenthesis)
{
    switch (parenthesis) {
        case ENUM_OPEN_PARENTHECE:
        case ENUM_CLOSE_PARENTHECE:
            return 1;
    }
    return 0;
}

static int
isOperator(char operator)
{
    switch (operator) {
        case ENUM_OP_MODULO:
        case ENUM_OP_DEVIDE:
        case ENUM_OP_MULTIPLY:
        case ENUM_OP_SUBTRACT:
        case ENUM_OP_SUM:
            return 1;
    }
    return 0;
}

static int
isOperand(char operand)
{
    return (((operand >= '0' && operand <= '9') ||
                (operand >= 'a' && operand <= 'z') ||
                (operand >= 'A' && operand <= 'Z')) ? 1 : 0);
}

static int
operatorWeight(char op)
{
    switch (op) {
        case ENUM_OP_MODULO:
            return ENUM_MODULO;
        case ENUM_OP_DEVIDE:
            return ENUM_DEVIDE;
        case ENUM_OP_MULTIPLY:
            return ENUM_MULTIPLY;
        case ENUM_OP_SUBTRACT:
            return ENUM_SUBTRACT;
        case ENUM_OP_SUM:
            return ENUM_SUM;
    }
    return 0;
}

static int
hasHigherPrecedence(char op1, char op2)
{
    int op1Weight = operatorWeight(op1);
    int op2Weight = operatorWeight(op2);

    return (op1Weight >= op2Weight ? 1 : 0);
}

static int
appendChar(char *str, char data)
{
    strncat(str, &data, 1);
}

static int
appendOperator(char stack[], int *head, char *str, char data)
{
    char top;
    if ((!isStackEmpty(*head)) && (topOfStack(stack, *head, &top)) && (hasHigherPrecedence(top, data))) {
        appendChar(str, top);
        pop(stack, head);
    }

    return ((isOperator(data)) && (push(stack, head, data)));
}

static int
appendParenthesis(char stack[], int *head, char *str, char data)
{
    char top;
    if ((!isStackEmpty(*head)) && (topOfStack(stack, *head, &top))) {
        if (data == ENUM_CLOSE_PARENTHECE) {
            while (top != ENUM_OPEN_PARENTHECE) {
                appendChar(str, top);
                pop(stack, head);
                topOfStack(stack, *head, &top);
            }
            pop(stack, head);
            return 1;
        }
    }
    return ((isParenthesis(data)) && (push(stack, head, data)));
}

static int
appendOperand(char *str, char data)
{
    appendChar(str, data);
}

static void
infixToPostfix(char infix[], char postfix[])
{
    char stackOp[BUFSIZE_1024] = {0};
    int i, infixLen, stackOpHead = 0;
    infixLen = strlen(infix);

    for (i = 0; i < infixLen; i++) {
        if (isOperator(infix[i])) {
            appendOperator(stackOp, &stackOpHead, postfix, infix[i]);
        } else if (isParenthesis(infix[i])) {
            appendParenthesis(stackOp, &stackOpHead, postfix, infix[i]);
        } else if (isOperand(infix[i])) {
            appendOperand(postfix, infix[i]);
        }
    }

    while(stackOpHead) {
        char top;
        topOfStack(stackOp, stackOpHead, &top);
        if (isOperand(top) || isOperator(top)) {
            appendChar(postfix, top);
        }
        pop(stackOp, &stackOpHead);
    }
}

static void
reverseExpr(char *str)
{
    char newStr[BUFSIZE_1024] = {0}, top;
    int i, strLen, strHead, newStrHead = 0;
    strHead = strLen = strlen(str);

    for (i = 0; i < strLen; i++) {
        topOfStack(str, strHead, &top);
        switch(top) {
            case ENUM_OPEN_PARENTHECE:
                push(newStr, &newStrHead, ENUM_CLOSE_PARENTHECE);
                break;
            case ENUM_CLOSE_PARENTHECE:
                push(newStr, &newStrHead, ENUM_OPEN_PARENTHECE);
                break;
            case ENUM_OPEN_BRACE:
                push(newStr, &newStrHead, ENUM_CLOSE_BRACE);
                break;
            case ENUM_CLOSE_BRACE:
                push(newStr, &newStrHead, ENUM_OPEN_BRACE);
                break;
            case ENUM_OPEN_BRACKET:
                push(newStr, &newStrHead, ENUM_CLOSE_BRACKET);
                break;
            case ENUM_CLOSE_BRACKET:
                push(newStr, &newStrHead, ENUM_OPEN_BRACKET);
                break;
            default:
                push(newStr, &newStrHead, top);
                break;
        }
        pop(str, &strHead);
    }
    strcpy(str, newStr);
}

static void
infixToPrefix(char infix[], char prefix[])
{
    char newInfix[BUFSIZE_1024];

    strcpy(newInfix, infix);
    reverseExpr(newInfix);
    infixToPostfix(newInfix, prefix);
    reverseExpr(prefix);
}

static void
trim(char *str)
{
    char len = strlen(str) - 1;
    if (str[len] == '\n') {
        str[len] = '\0';
    }
}

int main(int argc, char **argv)
{
    char infixExpr[BUFSIZE_1024] = {0};
    char prefixExpr[BUFSIZE_1024] = {0};
    char postfixExpr[BUFSIZE_1024] = {0};

    printf("Enter prefix Expression: ");
    fgets(infixExpr, BUFSIZE_1024, stdin);
    trim(infixExpr);

    infixToPrefix(infixExpr, prefixExpr);
    infixToPostfix(infixExpr, postfixExpr);

    printf("Infix Expr      : %s\n", infixExpr);
    printf("Prefix Expr     : %s\n", prefixExpr);
    printf("Postfix Expr    : %s\n", postfixExpr);

    return 0;
}
