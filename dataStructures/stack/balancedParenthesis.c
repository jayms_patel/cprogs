#include <stdio.h>
#include <string.h>

#define BUF_1024    1024

static int
isStackFull(int head)
{
    return ((head >= BUF_1024) ? 1 : 0);
}

static int
isStackEmpty(int head)
{
    return ((head <= 0) ? 1 : 0);
}

static int
push(char stack[], int *head, char data)
{
    if (isStackFull(*head)) {
        printf("Stack Overflow\n");
        return 1;
    }

    stack[(*head)++] = data;

    return 0;
}

static int
pop(char stack[], int *head)
{
    if (isStackEmpty(*head)) {
        printf("Stack Overflow\n");
        return 0;
    }

    --(*head);

    return 1;
}

static int
topOfStack(char stack[], int head, char *data)
{
    if (isStackEmpty(head)) {
        printf("Stack Overflow\n");
        return 0;
    }
    *data = stack[head - 1];
    return 1;
}

static int
checkBalancedParenthesis(char stack[], int *head, char data)
{
    char lastNode;

    if (isStackEmpty(*head)) {
        printf("Stack Overflow\n");
        return 0;
    }
    topOfStack(stack, *head, &lastNode);

    if (lastNode == data) {
        pop(stack, head);
        return 1;
    }
    return 0;
}

int main(int argc, char **argv)
{
    char expr[BUF_1024] = {0}, exprStack[BUF_1024] = {0};
    int i, exprLen = 0, head = 0, ret = 0;

    printf("Enter Expression: ");
    fgets(expr, BUF_1024 - 1, stdin);
    exprLen = strlen(expr) - 1;
    printf("Expression: %s", expr);

    for (i = 0; i < exprLen ; i++) {
        switch (expr[i]) {
            case '(':
            case '{':
            case '[':
                push(exprStack, &head, expr[i]);
                break;
            case ')':
                if (!(ret = checkBalancedParenthesis(exprStack, &head, '('))) {
                    goto exit;
                }
                break;
            case '}':
                if (!(ret = checkBalancedParenthesis(exprStack, &head, '{'))) {
                    goto exit;
                }
                break;
            case ']':
                if (!(ret = checkBalancedParenthesis(exprStack, &head, '['))) {
                    goto exit;
                }
                break;
        }
    }

exit:
    if (!ret || head) {
        printf("Expression is not Balanced\n");
        return 1;
    } else {
        printf("Expression is Balanced\n");
    }
    return 0;
}
