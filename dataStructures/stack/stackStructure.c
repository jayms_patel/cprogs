#include <stdio.h>
#include <stdlib.h>

struct stack {
    int data;
    struct stack *link;
};

static struct stack *
getNode()
{
    struct stack *temp;
    temp = (struct stack *) malloc(sizeof(struct stack));

    return temp;
}

static int
push(struct stack **s, int data)
{
    struct stack *node, *temp;

    temp = getNode();
    temp->data = data;
    temp->link = NULL;

    node = *s;
    if (!node) {
        *s = temp;
    } else {
        while (node->link) {
            node = node->link;
        }
        node->link = temp;
    }
    return 0;
}

static int
pop(struct stack **s)
{
    struct stack *node, *prev;

    node = *s;
    if (!node) {
        printf("Stack is empty\n");
    } else {
        while (node->link) {
            prev = node;
            node = node->link;
        }
        free(node);
        prev->link = NULL;
    }
    return 0;
}

static void
displayStack(struct stack *node)
{
    while (node) {
        printf("%d ", node->data);
        node = node->link ;
    }
}

static void
distroyList(struct stack *node)
{
    struct stack *next;

    while(node) {
        next = node->link;
        free(node);
        node = next;
    }
}

int main()
{
    int ch, data;
    struct stack *s = NULL;

    do {
        printf("\nMenu Selection\n");
        printf(" 1. Push node to stack\n");
        printf(" 2. Pop node from stack\n");
        printf(" 3. Display stack\n");
        printf(" 0. Exit\n");

        printf("Enter your selection: ");
        scanf("%d", &ch);

        switch(ch) {
            case 1:
                printf("Enter data: ");
                scanf("%d", &data);
                push(&s, data);
                break;
            case 2:
                pop(&s);
                break;
            case 3:
                displayStack(s);
                break;
            case 0:
                distroyList(s);
                break;
        }
    } while (ch != 0);
}
