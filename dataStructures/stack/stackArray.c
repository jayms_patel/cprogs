#include <stdio.h>

#define STACK_SIZE  5

static int
isFull(int top)
{
    if (top >= STACK_SIZE) {
        printf("Stack is full\n");
        return 1;
    }
    return 0;
}

static int
isEmpty(int top)
{
    if (top <= 0) {
        printf("Stack is empty\n");
        return 1;
    }
    return 0;
}

static int
push(int stack[], int data, int *top)
{
    if (isFull(*top)) {
        return 0;
    }

    stack[(*top)++] = data;
    return 1;
}

static int
pop(int stack[], int *top)
{
    if (isEmpty(*top)) {
        return 0;
    }
    (*top)--;
    return 1;
}

static int
displayStack(int stack[], int top)
{
    int i;

    if (isEmpty(top)) {
        return 0;
    }

    for (i = 0; i <top; i++) {
        printf("%d ", stack[i]);
    }
}

int main()
{
    int ch, data;
    int stack[STACK_SIZE], top = 0;

    do {
        printf("\nMenu Selection\n");
        printf(" 1. Push node to stack\n");
        printf(" 2. Pop node from stack\n");
        printf(" 3. Display stack\n");
        printf(" 0. Exit\n");

        printf("Enter your selection: ");
        scanf("%d", &ch);

        switch(ch) {
            case 1:
                printf("Enter data: ");
                scanf("%d", &data);
                push(stack, data, &top);
                break;
            case 2:
                pop(stack, &top);
                break;
            case 3:
                displayStack(stack, top);
                break;
            case 0:
                break;
        }
    } while (ch != 0);

    return 0;
}
