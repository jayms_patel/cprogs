#include <stdio.h>

#define ARRAY_SIZE  5

static void
merge(int l[], int lSize, int r[], int rSize, int a[], int aSize)
{
    int i, j, k;
    i = j = k = 0;

    while (i < lSize && j < rSize) {
        if (l[i] < r[j]) {
            a[k++] = l[i++];
        } else {
            a[k++] = r[j++];
        }
    }

    while (i < lSize) {
        a[k++] = l[i++];
    }
    while (j < rSize) {
        a[k++] = r[j++];
    }
}

static void
mergeSort(int a[], int n)
{
    int lSize = (n % 2) ? (n / 2) + 1:  (n / 2);
    int rSize = n - lSize;
    int left[lSize], right[rSize];
    int i, j, k;
    i = j = k = 0;

    if (n < 2) {
        return;
    }

    for (i = 0; i < lSize; i++) {
        left[i] = a[k++];
    }
    for (j = 0; j < rSize; j++) {
        right[j] = a[k++];
    }

    mergeSort(left, lSize);
    mergeSort(right, rSize);
    merge(left, lSize, right, rSize, a, n);
}

static void
printfArray(int a[], int n)
{
    int i;

    for (i = 0; i < n; i++) {
        printf("Value of a[%d]: %d\n", i, a[i]);
    }
}

int main(int argc, char **argv)
{
    int i, a[ARRAY_SIZE];

    for (i = 0; i < ARRAY_SIZE; i++) {
        printf("Enter value for a[%d]: ", i);
        scanf("%d", &a[i]);
    }

    mergeSort(a, ARRAY_SIZE);
    printfArray(a, ARRAY_SIZE);
}
