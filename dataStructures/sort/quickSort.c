#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE  5

static void
swap(int *a, int *b)
{
    int tmp;

    tmp = *a;
    *a = *b;
    *b = tmp;
}

static int
partitionArray(int a[], int start, int end)
{
    int pivot = a[end];
    int pIndex = start;
    int i;

    for (i = start; i < end; i++) {
        if (a[i] <= pivot) {
            swap(&a[i], &a[pIndex]);
            pIndex++;
        }
    }
    swap(&a[pIndex], &a[end]);
    return pIndex;
}

static int
randomizePartitionArray(int a[], int start, int end)
{
    int pIndex = (rand() % (end - start + 1) + start);
    swap(&a[pIndex], &a[end]);
    return (partitionArray(a, start, end));
}

static void
quickSort(int a[], int start, int end)
{
    int pIndex = 0;

    if (start < end) {
        pIndex = randomizePartitionArray(a, start, end);
        quickSort(a, start, pIndex -1);
        quickSort(a, pIndex, end);
    }
}

static void
printfArray(int a[], int n)
{
    int i;

    for (i = 0; i < n; i++) {
        printf("Value of a[%d]: %d\n", i, a[i]);
    }
}

int main(int argc, char **argv)
{
    int i, a[ARRAY_SIZE];

    for (i = 0; i < ARRAY_SIZE; i++) {
        printf("Enter value for a[%d]: ", i);
        scanf("%d", &a[i]);
    }

    quickSort(a, 0, ARRAY_SIZE - 1);
    printfArray(a, ARRAY_SIZE);
}
