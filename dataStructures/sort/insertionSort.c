#include <stdio.h>

#define ARRAY_SIZE  5

static void
insertionSort(int a[], int n)
{
    int i, j, value, hole;

    for (i = 1; i < n; i++) {
        value = a[i];
        hole = i;
        while (hole > 0 && a[hole -1] > value) {
            a[hole] = a[hole - 1];
            hole--;
        }
        a[hole] = value;
    }
}

static void
printfArray(int a[], int n)
{
    int i;

    for (i = 0; i < n; i++) {
        printf("Value of a[%d]: %d\n", i, a[i]);
    }
}

int main(int argc, char **argv)
{
    int i, a[ARRAY_SIZE];

    for (i = 0; i < ARRAY_SIZE; i++) {
        printf("Enter value for a[%d]: ", i);
        scanf("%d", &a[i]);
    }

    insertionSort(a, ARRAY_SIZE);
    printfArray(a, ARRAY_SIZE);
}
