#include <stdio.h>

#define ARRAY_SIZE  5

static void
swapByReference(int *a, int *b)
{
    int temp = *a;

    *a = *b;
    *b = temp;
}

static void
bubbleSort(int a[], int n)
{
    int i, j, temp;

    for (i = 0; i < n; i++) {
        int isSwapped = 0;
        for (j = 0; j < (n-i-1); j++) {
            if (a[j] > a[j+1]) {
                swapByReference(&a[j], &a[j+1]);
                isSwapped = 1;
            }
        }
        if (!isSwapped) {
            break;
        }
    }
}

static void
printfArray(int a[], int n)
{
    int i;

    for (i = 0; i < n; i++) {
        printf("Value of a[%d]: %d\n", i, a[i]);
    }
}

int main(int argc, char **argv)
{
    int i, a[ARRAY_SIZE];

    for (i = 0; i < ARRAY_SIZE; i++) {
        printf("Enter value for a[%d]: ", i);
        scanf("%d", &a[i]);
    }

    bubbleSort(a, ARRAY_SIZE);
    printfArray(a, ARRAY_SIZE);
}
