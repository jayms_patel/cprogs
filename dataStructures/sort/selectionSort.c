#include <stdio.h>

#define ARRAY_SIZE  5

static void
selectionSort(int a[], int n)
{
    int i, j, iMin, temp;

    for (i = 0; i < (n-1); i++) {
        iMin = i;
        for (j = (i+1); j < n; j++) {
            if (a[j] < a[iMin]) {
                iMin = j;
            }
        }
        temp = a[i];
        a[i] = a[iMin];
        a[iMin] = temp;
    }
}

static void
printfArray(int a[], int n)
{
    int i;

    for (i = 0; i < n; i++) {
        printf("Value of a[%d]: %d\n", i, a[i]);
    }
}

int main(int argc, char **argv)
{
    int i, a[ARRAY_SIZE];

    for (i = 0; i < ARRAY_SIZE; i++) {
        printf("Enter value for a[%d]: ", i);
        scanf("%d", &a[i]);
    }

    selectionSort(a, ARRAY_SIZE);
    printfArray(a, ARRAY_SIZE);
}
