#include <stdio.h>
#include <stdlib.h>

struct structQueue {
    int data;
    struct structQueue *link;
};

static struct structQueue *
getNode(void)
{
    struct structQueue *tmp;

    tmp = (struct structQueue *) malloc(sizeof(struct structQueue));

    return tmp;
}

static int
queueEmpty(struct structQueue *front, struct structQueue *rear)
{
    if (front == NULL && rear == NULL) {
        return 1;
    }
    return 0;
}

static void
enqueue(struct structQueue **queue, int data, struct structQueue **front, struct structQueue **rear)
{
    struct structQueue *tmp = getNode();

    tmp->data = data;

    if (queueEmpty(*front, *rear)) {
        tmp->link = tmp;
        *queue = *front = *rear = tmp; 
        return;
    }

    tmp->link = *front;
    (*rear)->link = tmp;
    *rear = tmp;
}

static void
dequeue(struct structQueue **queue, struct structQueue **front, struct structQueue **rear)
{
    struct structQueue *tmp;

    if (queueEmpty(*front, *rear)) {
        printf("Queue is empty\n");
        return;
    }

    if (*front == *rear) {
        free(*queue);
        *queue = *front = *rear = NULL;
    } else {
        tmp = *front;
        *front = tmp->link;
        (*rear)->link = *front;
        *queue = *front;
        free(tmp);
    }
}

static void
displayQueue(struct structQueue *queue, struct structQueue *front, struct structQueue *rear)
{
    if (queueEmpty(front, rear)) {
        printf("Queue is empty\n");
        return;
    }

    do {
        printf("%d ", queue->data);
        queue = queue->link;
    } while (queue != front);
}

static void
distroyQueue(struct structQueue *queue, struct structQueue *front, struct structQueue *rear)
{
    struct structQueue *tmp;

    if (queueEmpty(front, rear)) {
        printf("Queue is empty\n");
        return;
    }

    do {
        tmp = queue;
        queue = queue->link;
        free(tmp);
    } while (queue != front);
}

int main(int argc, char *argv[])
{
    int ch, data;
    struct structQueue *queue, *front, *rear;
    queue = front = rear = NULL;

    do {
        printf("\nMenu Selection\n");
        printf(" 1. Insert node to queue\n");
        printf(" 2. Delete node from queue\n");
        printf(" 3. Display queue\n");
        printf(" 0. Exit\n");

        printf("Enter your selection : ");
        scanf("%d", &ch);

        switch(ch) {
            case 1:
                printf("Enter Data: ");
                scanf("%d", &data);
                enqueue(&queue, data, &front, &rear);
                break;
            case 2:
                dequeue(&queue, &front, &rear);
                break;
            case 3:
                displayQueue(queue, front, rear);
                break;
            case 0:
                distroyQueue(queue, front, rear);
                break;
        }
    } while (ch != 0);
    return 0;
}
