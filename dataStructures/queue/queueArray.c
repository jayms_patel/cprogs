#include <stdio.h>

#define QUEUE_SIZE  5

static int
queueFull(int rear)
{
    if (rear < QUEUE_SIZE - 1) {
        return 0;
    }
    return 1;
}

static int
queueEmpty(int front, int rear)
{
    if (front == -1 && rear == -1) {
        return 1;
    }
    return 0;
}

static void
enqueue(int queue[], int data, int *front, int *rear)
{
    if (queueFull(*rear)) {
        printf("Queue Overflow\n");
        return;
    }

    if (*front == -1 && *rear == -1) {
        *front = *rear = 0; 
    } else {
        ++*rear;
    }
    queue[*rear] = data;
}

static void
dequeue(int queue[], int *front, int *rear)
{
    if (queueEmpty(*front, *rear)) {
        printf("Queue is empty\n");
        return;
    }

    if (*front == *rear) {
        *front = *rear = -1;
    } else {
        ++*front;
    }
}

static void
displayQueue(int queue[], int front, int rear)
{
    int i;
    for (i = front; i <= rear; i++) {
        printf("%d ", queue[i]);
    }
}

int main(int argc, char *argv[])
{
    int ch, data;
    int queue[QUEUE_SIZE], front, rear;
    front = rear = -1;

    do {
        printf("\nMenu Selection\n");
        printf(" 1. Insert node to queue\n");
        printf(" 2. Delete node from queue\n");
        printf(" 3. Display queue\n");
        printf(" 0. Exit\n");

        printf("Enter your selection : ");
        scanf("%d", &ch);

        switch(ch) {
            case 1:
                printf("Enter Data: ");
                scanf("%d", &data);
                enqueue(queue, data, &front, &rear);
                break;
            case 2:
                dequeue(queue, &front, &rear);
                break;
            case 3:
                displayQueue(queue, front, rear);
                break;
            case 0:
                break;
        }
    } while (ch != 0);
    return 0;
}
