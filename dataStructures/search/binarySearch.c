/*
 * precondition: binary search needs sorted array
 * This program needs sorted input to work properly
 *
 */
#include <stdio.h>

#define ARRAY_SIZE      5

enum searchType {
    SEARCH_NORMAL   =   0,
    SEARCH_FIRST    =   1,
    SEARCH_LAST     =   2
};

static int
binarySearchRecursive(int a[], int data, int *index, int start, int end, int order)
{
    int mid, isFound = 0;

    while (start <= end) {
        mid = (start + end) / 2;
        if (a[mid] == data) {
            isFound = 1;
            *index = mid;
            if (order == SEARCH_FIRST) {
                end = mid - 1;
            } else if (order == SEARCH_LAST) {
                start = mid + 1;
            } else {
                return isFound;
            }
        } else if (a[mid] > data) {
            return binarySearchRecursive(a, data, index, start, mid - 1, order);
        } else {
            return binarySearchRecursive(a, data, index, mid + 1, end, order);
        }
    }
    return isFound;
}

static int
binarySearchIterative(int a[], int data, int *index, int start, int end, int order)
{
    int mid, isFound = 0;

    while (start <= end) {
        mid = (start + end) / 2;
        if (a[mid] == data) {
            isFound = 1;
            *index = mid;
            if (order == SEARCH_FIRST) {
                end = mid - 1;
            } else if (order == SEARCH_LAST) {
                start = mid + 1;
            } else {
                return isFound;
            }
        } else if (a[mid] > data) {
            end = mid - 1;
        } else {
            start = mid + 1;
        }
    }
    return isFound;
}

static void
printArray(int a[], int n)
{
    int i;

    for (i = 0; i < n; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    int a[ARRAY_SIZE];
    int i, data, firstIndex = 0, lastIndex = 0, count = 0, ch, ret;

    do {
        printf("Menu Selection\n\n");
        printf("1. Binary Search (Iterative Method)\n");
        printf("2. Binary Search (Recursive Method)\n");
        printf("3. Search first occurance\n");
        printf("4. Search last occurance\n");
        printf("5. Find number of occurance\n");
        printf("0. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &ch);

        if (ch) {
            for (i = 0; i < ARRAY_SIZE; i++) {
                printf("Enter value of a[%d]: ", i);
                scanf("%d", &a[i]);
            }
            printf("Enter value to search: ");
            scanf("%d", &data);
        }

        switch (ch) {
            default:
            case 1:
                ret = binarySearchIterative(a, data, &firstIndex, 0, ARRAY_SIZE, SEARCH_NORMAL);
                if (ret) {
                    printf("%d found at index %d\n", data, firstIndex);
                }
                break;
            case 2:
                ret = binarySearchRecursive(a, data, &firstIndex, 0, ARRAY_SIZE, SEARCH_NORMAL);
                if (ret) {
                    printf("%d found at index %d\n", data, firstIndex);
                }
                break;
            case 3:
                ret = binarySearchIterative(a, data, &firstIndex, 0, ARRAY_SIZE, SEARCH_FIRST);
                if (ret) {
                    printf("%d found at index %d\n", data, firstIndex);
                }
                break;
            case 4:
                ret = binarySearchIterative(a, data, &lastIndex, 0, ARRAY_SIZE, SEARCH_LAST);
                if (ret) {
                    printf("%d found at index %d\n", data, lastIndex);
                }
                break;
            case 5:
                if ((ret = binarySearchIterative(a, data, &firstIndex, 0, ARRAY_SIZE, SEARCH_FIRST))) {
                    ret = binarySearchIterative(a, data, &lastIndex, 0, ARRAY_SIZE, SEARCH_LAST);
                    count = (lastIndex - firstIndex) + 1;
                    printf("first: %d, last: %d\n", firstIndex, lastIndex);
                    printf("%d has %d number of occurances\n", data, count);
                }
                break;
            case 0:
                return 0;
        }
    } while (ch != 0);
}
