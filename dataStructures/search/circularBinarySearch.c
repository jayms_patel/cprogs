/*
 * precondition: binary search needs sorted array
 * Program assumes input is sorted or circular sorted array
 * This program does not work properly with duplicated values
 * program search for data in circular sorted array
 */


#include <stdio.h>

#define ARRAY_SIZE  5

static int
circularBinarySearch(int a[], int n, int *index, int data)
{
    int mid, low = 0, high = n - 1, prev, next;

    while (low <= high) {
        mid = (low + high) / 2;
        if (data == a[mid]) {
            *index = mid;
            return 1;
        }
        if (a[mid] <= data && data <= a[high]) {
            low = (mid + 1) % n;
        } else {
            high = (mid - 1 + n) % n;
        }
    }
}

int main(int argc, char **argv)
{
    int i, a[ARRAY_SIZE], data, index;

    for (i = 0; i < ARRAY_SIZE; i++) {
        printf("Enter value of a[%d]: ", i);
        scanf("%d", &a[i]);
    }

    printf("Enter value to search: ");
    scanf("%d", &data);

    if (circularBinarySearch(a, ARRAY_SIZE, &index, data)) {
        printf("%d found at index %d\n", data, index);
    }

    return 0;
}
