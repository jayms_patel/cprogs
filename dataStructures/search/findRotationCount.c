/*
 * precondition: binary search needs sorted array
 * Program assumes input is sorted or circular sorted array
 * This program does not work properly with duplicated values
 * From circulay array input program shows number of repeatations in
 * sorted array
 */

#include <stdio.h>

#define ARRAY_SIZE  5

static int
findRotationCount(int a[], int n)
{
    int low = 0, high = n - 1, mid, next, prev;

    while (low <= high) {
        if (a[low] <= a[high]) {
            return low;
        }
        mid = (low + high) / 2;
        prev = (mid - 1 + n) % n;
        next = (mid + 1) % n;  
        if (a[mid] <= a[prev] && a[mid] <= a[next]) { 
            return mid;
        } else if (a[mid] <= a[high]) {
            high = mid - 1;
        } else if (a[low] <= a[mid]) {
            low = mid + 1;
        }
    }

    return -1;
}

int main(int argc, char **argv)
{
    int i, a[ARRAY_SIZE], rCount = 0;

    for (i = 0; i < ARRAY_SIZE; i++) {
        printf("Enter value of a[%d]: ", i);
        scanf("%d", &a[i]);
    }

    rCount = findRotationCount(a, ARRAY_SIZE);
    printf("Array rotated for %d times\n", rCount);

    return 0;
}
