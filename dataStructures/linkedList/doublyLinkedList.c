#include <stdio.h>
#include <stdlib.h>

struct linkedList {
	int data;
	struct linkedList *next, *prev;
};

static void
insertNodeAtBegin(struct linkedList **rootNode, int data)
{
	struct linkedList *tmp = NULL, *node;
	node = *rootNode;

	tmp = (struct linkedList *) malloc(sizeof(struct linkedList));
	tmp->prev = NULL;
	tmp->data = data;
	tmp->next = node;
	if (node) {
		node->prev = tmp;
	}

	*rootNode = tmp;
}

static void
insertNodeAtEnd(struct linkedList **rootNode, int data)
{
	struct linkedList *tmp = NULL, *node;
	node = *rootNode;

	tmp = (struct linkedList *) malloc(sizeof(struct linkedList));
	tmp->data = data;

	if (!node) {
		tmp->next = tmp->prev = NULL;
		*rootNode = tmp;
		return;
	}

	while (node && node->next) {
		node = node->next;
	}

	node->next = tmp;
	tmp->prev = node;
	tmp->next = NULL;
}

static int
removeNode(struct linkedList **rootNode, int data)
{
	struct linkedList *lastNode = NULL, *nextNode = NULL, *node;
	node = *rootNode;

	while (node) {
		lastNode = node->prev;
		nextNode = node->next;
		if (node->data == data) {
			if (lastNode) {
				lastNode->next = nextNode;
			} else {
				*rootNode = nextNode;
			}
			if (nextNode) {
				nextNode->prev = lastNode;
			} else {
				nextNode = NULL;
			}
			free(node);
			return 1;
		}
		node = nextNode;;
	}
	return -1;
}

static void
displayList(struct linkedList *node)
{
	while (node) {
		printf("%d ", node->data);
		node = node->next;
	}
}

static void
displayReverseList(struct linkedList *node)
{
	while (node && node->next) {
		node = node->next;
	}

	while (node) {
		printf("%d ", node->data);
		node = node->prev;
	}
}

static void
distroyList(struct linkedList *node)
{
	struct linkedList *tmp = NULL;

	while (node) {
		tmp = node->next;
		free(node);
		node = tmp;
	}
}

int main()
{
	int ch, data;
	struct linkedList *rootNode;

	do {
		printf("\nMenu Selection\n");
		printf(" 1. Insert node at beginning\n");
		printf(" 2. Insert node at end\n");
		printf(" 3. Remove Node\n");
		printf(" 4. Display linked list\n");
		printf(" 5. Display linked list in reverse order\n");
		printf(" 0. Exit\n");

		printf("Enter you selection: ");
		scanf("%d", &ch);

		switch(ch) {
			case 0:
				distroyList(rootNode);
				break;
			case 1:
				printf("Enter Data: ");
				scanf("%d", &data);
				insertNodeAtBegin(&rootNode, data);
				break;
			case 2:
				printf("Enter Data: ");
				scanf("%d", &data);
				insertNodeAtEnd(&rootNode, data);
				break;
			case 3:
				printf("Enter Data: ");
				scanf("%d", &data);
				if (removeNode(&rootNode, data) == -1) {
					printf("Node does not exist\n");
				}
				break;
			case 4:
				displayList(rootNode);
				break;
			case 5:
				displayReverseList(rootNode);
				break;
			default:
				break;
		}
	} while (ch !=0 );

	return 0;
}
