#include <stdio.h>
#include <stdlib.h>

struct linkedList {
	int data;
	struct linkedList *link;
};

static void
insertNodeAtBegin(struct linkedList **node, int data)
{
	struct linkedList *tmp;

	tmp = (struct linkedList *) malloc(sizeof(struct linkedList));
	tmp->data = data;
	tmp->link = *node;
	*node = tmp;

	return;
}

static void
insertNodeAtEnd(struct linkedList **node, int data)
{
	struct linkedList *tmp, *tmpRoot;

	tmp = (struct linkedList *) malloc(sizeof(struct linkedList));
	tmp->data = data;
	tmp->link = NULL;

	tmpRoot = *node;
	if (!tmpRoot) {
		*node = tmp;
		return;
	}

	while (tmpRoot && tmpRoot->link) {
		tmpRoot = tmpRoot->link;
	}
	tmpRoot->link = tmp;

	return;
}

static void
displayList(struct linkedList *node)
{
	while (node) {
		printf("%d ", node->data);
		node = node->link;
	}

	return;
}

static void
searchNode(struct linkedList *node, int data)
{
	while(node) {
		if (node->data == data) {
			printf("Data exist\n");
			return;
		}
		node = node->link;
	}
	printf("Data does not exist\n");
}

static void
removeNode(struct linkedList **rootNode, int data)
{
	struct linkedList *removeNode = NULL, *lastNode = NULL, *node;
	node = *rootNode;

	while(node) {
		if (node->data == data) {
			removeNode = node;
			if (!lastNode) {
				*rootNode = removeNode->link;
			} else {
				lastNode->link = removeNode->link;
			}
			free(removeNode);
			return;
		}
		lastNode = node;
		node = node->link;
	}
}

static void
distroyList(struct linkedList *node)
{
	struct linkedList *tmp;

	while (node) {
		tmp = node;
		node = node->link;
		free(tmp);
	}
	return;
}

static void
reverseLinkedList(struct linkedList **rootNode)
{
	struct linkedList *node, *lastNode = NULL, *nextNode;
	node = *rootNode;
	nextNode = node;

	while (node) {
		nextNode = node->link;
		node->link = lastNode;
		lastNode = node;
		node = nextNode;
	}
	*rootNode = lastNode;

	return;
}

static void
reverseLinkedListRecursive(struct linkedList *node)
{
	if (node) {
		reverseLinkedListRecursive(node->link);
		printf("%d ", node->data);
	}

	return;
}

int main()
{
	int ch, data;
	struct linkedList *rootNode = NULL;

	do {
		printf("\n\nMenu Selection\n");
		printf(" 1. Insert node at beginning\n");
		printf(" 2. Insert node at end\n");
		printf(" 3. Search node in list\n");
		printf(" 4. Remove node\n");
		printf(" 5. Display List\n");
		printf(" 6. Reverse linked list (Iterative Method)\n");
		printf(" 7. Reverse linked list (Recursive Method)\n");
		printf(" 0. Exit\n");

		printf("Enter your selection: ");
		scanf("%d", &ch);

		switch (ch) {
			case 0:
				distroyList(rootNode);
				break;
			case 1:
				printf("Enter data: ");
				scanf("%d", &data);
				insertNodeAtBegin(&rootNode, data);
				break;
			case 2:
				printf("Enter data: ");
				scanf("%d", &data);
				insertNodeAtEnd(&rootNode, data);
				break;
			case 3:
				printf("Enter data: ");
				scanf("%d", &data);
				searchNode(rootNode, data);
				break;
			case 4:
				printf("Enter data: ");
				scanf("%d", &data);
				removeNode(&rootNode, data);
				break;
			case 5:
				displayList(rootNode);
				break;
			case 6:
				reverseLinkedList(&rootNode);
				break;
			case 7:
				reverseLinkedListRecursive(rootNode);
				break;
			default:
				break;
		}
	} while (ch != 0);

	return 0;
}
