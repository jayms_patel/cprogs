#include <stdio.h>
#include <stdlib.h>

struct linkedList {
	int data;
	struct linkedList *link;
};

static struct linkedList *
getNode()
{
    struct linkedList *node;

    node = (struct linkedList *) (calloc(1, sizeof(struct linkedList)));
    return node;
}

static void
insertNodeAtBegin(struct linkedList **lastNode, int data)
{
    struct linkedList *head, *last, *node;
    node = getNode();
    node->data = data;
    if (!(*lastNode)) {
        node->link = node;
        *lastNode = node;
        return;
    } else {
        last = *lastNode;
        head = last->link;
        last->link = node;
        node->link = head;
    }
}

static void
insertNodeAtEnd(struct linkedList **lastNode, int data)
{
    struct linkedList *head, *last, *node;
    node = getNode();
    node->data = data;
    if (!(*lastNode)) {
        node->link = node;
    } else {
        last = *lastNode;
        head = last->link;
        last->link = node;
        node->link = head;
    }
    *lastNode = node;
}

static void
displayList(struct linkedList *last)
{
    struct linkedList *head, *temp;

    if (!last) {
        printf("List is empty\n");
        return;
    }

    temp = head = last->link;
    do {
        printf("%d ", temp->data);
        temp = temp->link;
    } while (temp != head);
}

static void
searchNode(struct linkedList *last, int data)
{
    struct linkedList *temp, *head;

    if (!last) {
        printf("List is empty\n");
        return;
    }

    temp = head = last->link;
    do {
        if (temp->data == data) {
            printf("Data exist\n");
            return;
        }
        temp = temp->link;
    } while (temp != head);
    printf("Data does not exist\n");
}

static void
removeNode(struct linkedList **lastNode, int data)
{
    struct linkedList *last, *next, *head, *temp, *prev;

    prev = last = *lastNode;
    if (!last) {
        printf("List is empty\n");
        return;
    }

    temp = head = last->link;
    do {
        next = temp->link;
        if (temp->data == data) {
            if ((temp->link == head) && (temp->link == last)) {
                *lastNode = NULL;
                free(temp);
                return;
            } else {
                prev->link = next;
                free(temp);
                return;
            }
        }
        prev = temp;
        temp =  next; 
    } while (temp != head);
}

static void
distroyList(struct linkedList *last)
{
    struct linkedList *temp, *head, *next;

    if (!last) {
        printf("List is empty\n");
        return;
    }

    temp = head = last->link;
    do {
        next = temp->link;
        free(temp);
        temp = next;
    } while (temp != head);
}

static void
reverseLinkedList(struct linkedList **lastNode)
{
}

static void
reverseLinkedListRecursive(struct linkedList *last)
{
}

int main(void)
{
	int ch, data;
	struct linkedList *lastNode = NULL;

	do {
		printf("\n\nMenu Selection\n");
		printf(" 1. Insert node at beginning\n");
		printf(" 2. Insert node at end\n");
		printf(" 3. Search node in list\n");
		printf(" 4. Remove node\n");
		printf(" 5. Display List\n");
		printf(" 6. Reverse linked list (Iterative Method)\n");
		printf(" 7. Reverse linked list (Recursive Method)\n");
		printf(" 0. Exit\n");

		printf("Enter your selection: ");
		scanf("%d", &ch);

		switch (ch) {
			case 0:
				distroyList(lastNode);
				break;
			case 1:
				printf("Enter data: ");
				scanf("%d", &data);
				insertNodeAtBegin(&lastNode, data);
				break;
			case 2:
				printf("Enter data: ");
				scanf("%d", &data);
				insertNodeAtEnd(&lastNode, data);
				break;
			case 3:
				printf("Enter data: ");
				scanf("%d", &data);
				searchNode(lastNode, data);
				break;
			case 4:
				printf("Enter data: ");
				scanf("%d", &data);
				removeNode(&lastNode, data);
				break;
			case 5:
				displayList(lastNode);
				break;
			case 6:
				reverseLinkedList(&lastNode);
				break;
			case 7:
				reverseLinkedListRecursive(lastNode);
				break;
			default:
				break;
		}
	} while (ch != 0);

	return 0;
}
